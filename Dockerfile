FROM localhost:5000/cuda11.8-jdk8:v1

WORKDIR /usr/src/app
ARG JAR_FILE
ARG UID_GID=1001
ENV UID=${UID_GID}
ENV GID=${UID_GID}

RUN mkdir -p jars
COPY jars jars

ENV PYTHONUNBUFFERED=1

RUN apt-get update && apt install -y python-is-python3 wget curl ca-certificates bash libgomp1 && \
    rm -rf /var/cache/apt/*

RUN mkdir -p /opt/spark/
COPY test_datasets /opt/spark

COPY ${JAR_FILE} service.jar

RUN groupadd --gid $UID appuser && useradd --uid $UID --gid appuser --shell /bin/bash --create-home appuser
RUN chown -R appuser:appuser /home/appuser && chown -R appuser:appuser /usr/src/app

EXPOSE 4040
EXPOSE 9090
USER $UID
CMD ["java", "-jar", "service.jar"]
