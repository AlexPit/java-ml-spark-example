create schema instrument_data;

create table instrument_data.models
(
    id              serial    primary key,
    model           bytea     not null,
    parameters      jsonb     not null,
    created_at      timestamp not null,
    last_trained_at timestamp not null,
    task_id         uuid      not null
);

alter table instrument_data.models
    owner to superadmin;
