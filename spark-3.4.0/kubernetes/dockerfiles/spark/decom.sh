#!/usr/bin/env bash

set +e
set -x
echo "Asked to decommission"
# Find the pid to signal
date | tee -a ${LOG}
WORKER_PID=$(ps -o pid,cmd -C java |grep Executor \
	       | tail -n 1| awk '{ sub(/^[ \t]+/, ""); print }' \
	       | cut -f 1 -d " ")
echo "Using worker pid $WORKER_PID"
kill -s SIGPWR ${WORKER_PID}
# If the worker does exit stop blocking K8s cleanup. Note this is a "soft"
# block since the pod it's self will have a maximum decommissioning time which will
# overload this.
echo "Waiting for worker pid to exit"
tail --pid=${WORKER_PID} -f /dev/null
sleep 1
date
echo "Done"
date
sleep 1
