#!/bin/bash

SPARK_DRIVER_BIND_ADDRESS=127.0.0.1:7077
NCCL_DEBUG=INFO

source ~/.bashrc

start-master.sh
start-worker.sh spark://$SPARK_DRIVER_BIND_ADDRESS
tail -f /dev/null
