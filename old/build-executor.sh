#!/bin/bash

docker build -f kubernetes/dockerfiles/spark/Dockerfile --build-arg java_image_tag=localhost:5000/cuda-jdk8:v1 -t abvgdeej/cuda-spark:v2 .
docker push abvgdeej/cuda-spark:v2
