package com.mlwebservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.evaluation.RegressionEvaluator;
import org.apache.spark.ml.feature.StandardScaler;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.regression.RandomForestRegressor;
import org.apache.spark.ml.tuning.CrossValidator;
import org.apache.spark.ml.tuning.CrossValidatorModel;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.stereotype.Service;

import static org.apache.spark.sql.functions.col;

@Slf4j
@Service
@RequiredArgsConstructor
public class SparkMLService {
    private final SparkSession session;

    public void randomForestTest() {
        String trainPath = "/opt/spark/train/train.parquet";
        String evalPath = "/opt/spark/eval/eval.parquet";

        Dataset<Row> tdf = session.read().parquet(trainPath);
        Dataset<Row> edf = session.read().parquet(evalPath);

        String labelName = "fare_amount";
        String[] featureColumns = {"passenger_count", "trip_distance", "pickup_longitude", "pickup_latitude", "rate_code",
                "dropoff_longitude", "dropoff_latitude", "hour", "day_of_week", "is_weekend", "h_distance"};

        VectorAssembler assembler = new VectorAssembler()
                .setInputCols(featureColumns)
                .setOutputCol("rawfeatures");

        StandardScaler standardScaler = new StandardScaler()
                .setInputCol("rawfeatures")
                .setOutputCol("features")
                .setWithStd(true);

        RandomForestRegressor regressor = new RandomForestRegressor()
                .setLabelCol(labelName)
                .setFeaturesCol("features");

        Pipeline pipeline = new Pipeline().setStages(new PipelineStage[]{assembler, standardScaler, regressor});

        ParamMap[] paramGrid = new ParamGridBuilder()
                .addGrid(regressor.maxBins(), new int[]{100, 200})
                .addGrid(regressor.maxDepth(), new int[]{2, 7, 10})
                .addGrid(regressor.numTrees(), new int[]{5, 20})
                .build();

        RegressionEvaluator evaluator = new RegressionEvaluator()
                .setLabelCol(labelName)
                .setPredictionCol("prediction")
                .setMetricName("rmse");

        CrossValidator crossvalidator = new CrossValidator()
                .setEstimator(pipeline)
                .setEvaluator(evaluator)
                .setEstimatorParamMaps(paramGrid)
                .setNumFolds(3);

        CrossValidatorModel pipelineModel = crossvalidator.fit(tdf);

        ParamMap[] bestEstimatorParamMap = pipelineModel.getEstimatorParamMaps();

        log.info("best params map {}", bestEstimatorParamMap);

        Dataset<Row> predictions = pipelineModel.transform(edf);
        Dataset<Row> result = predictions.withColumn("error", col("prediction").minus(col(labelName)));
        result.select(labelName, "prediction", "error").show();
        result.describe(labelName, "prediction", "error").show();

        RegressionEvaluator maevaluator = new RegressionEvaluator()
                .setLabelCol(labelName)
                .setMetricName("mae");
        log.info("mae evaluation: {}", maevaluator.evaluate(predictions));

        RegressionEvaluator rmsevaluator = new RegressionEvaluator()
                .setLabelCol(labelName)
                .setMetricName("rmse");
        log.info("rmse evaluation: {}", rmsevaluator.evaluate(predictions));
    }
}
