package com.mlwebservice.service;

import com.mlwebservice.config.SparkSettings;
import com.mlwebservice.model.LongValue;
import lombok.extern.slf4j.Slf4j;
import ml.dmlc.xgboost4j.scala.spark.XGBoostRegressionModel;
import ml.dmlc.xgboost4j.scala.spark.XGBoostRegressor;
import org.apache.spark.ml.PredictionModel;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import scala.collection.immutable.HashMap;
import scala.collection.immutable.Map;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.spark.sql.functions.col;

@Slf4j
@Service
public class RapidsService {

    private final SparkSettings sparkSettings;
    private SparkSession session;

    @Autowired
    @Profile("cluster")
    public void setSession(SparkSession session) {
        this.session = session;
    }

    public RapidsService(SparkSettings sparkSettings) {
        this.sparkSettings = sparkSettings;
    }

    public void testRapids() {
        int capacity = 1000000;
        List<LongValue> list = new ArrayList<>(capacity);
        for (long i = 1; i < (capacity + 1); i++) {
            list.add(new LongValue(i));
        }

        Dataset<Row> df = session.createDataFrame(list, LongValue.class);
        Dataset<Row> df2 = session.createDataFrame(list, LongValue.class);

        long result = df.select(col("value").as("a"))
                .join(df2.select(col("value").as("b")), col("a").equalTo(col("b"))).count();

        log.info("count result {}", result);
    }

    public void xgBoost() {
        String trainPath = "/opt/spark/train/train.parquet";
        String evalPath = "/opt/spark/eval/eval.parquet";

        Dataset<Row> tdf = session.read().parquet(trainPath);
        Dataset<Row> edf = session.read().parquet(evalPath);

        String labelName = "fare_amount";
        String[] featureColumns = {"passenger_count", "trip_distance", "pickup_longitude", "pickup_latitude", "rate_code",
                "dropoff_longitude", "dropoff_latitude", "hour", "day_of_week", "is_weekend", "h_distance"};

        Map<String, Object> map = new HashMap<>();
        map = map.updated("learning_rate", 0.05);
        map = map.updated("max_depth", 8);
        map = map.updated("subsample", 0.8);
        map = map.updated("gamma", 1);
        map = map.updated("num_round", 500);
        map = map.updated("tree_method", "gpu_hist");
        map = map.updated("num_workers", sparkSettings.getExecutorsNum());

        XGBoostRegressor regressor = new XGBoostRegressor(map);
        regressor.setLabelCol(labelName);
        regressor.setFeaturesCol(featureColumns);

        PredictionModel<Vector, XGBoostRegressionModel> model = regressor.fit(tdf);
        Dataset<Row> predictions = model.transform(edf);

        Dataset<Row> result = predictions.withColumn("error", col("prediction").minus(col(labelName)));
        result.select(labelName, "prediction", "error").show();
        result.describe(labelName, "prediction", "error").show();
    }
}
