package com.mlwebservice.utils;

import com.mlwebservice.config.SparkSettings;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.net.UnknownHostException;

@Slf4j
@UtilityClass
public class SparkUtils {

    public static SparkSession sparkSession(SparkSettings sparkSettings) throws UnknownHostException {
        JavaSparkContext context = javaSparkContext(sparkSettings);
        return sparkSession(sparkSettings, context);
    }

    public static SparkSession sparkSession(SparkSettings sparkSettings, JavaSparkContext context) {
        return SparkSession.builder()
                .master(sparkSettings.getMasterHost())
                .appName(sparkSettings.getAppName())
                .config(context.getConf())
                .config("spark.executorEnv.NCCL_DEBUG", "INFO")
                .config("spark.submit.deployMode", sparkSettings.getDeployMode())
                .getOrCreate();
    }

    public static JavaSparkContext javaSparkContext(SparkSettings sparkSettings) {
        String host = sparkSettings.getHost();
        String hostName = sparkSettings.getHostName();
        String deployMode = sparkSettings.getDeployMode();
        String appName = sparkSettings.getAppName();
        String masterHost = sparkSettings.getMasterHost();
        String executorsNum = sparkSettings.getExecutorsNum();
        String gpuPerTask = sparkSettings.getGpuPerTask();
        String namespace = sparkSettings.getNamespace();
        String driverSa = sparkSettings.getDriverSa();

        log.info("Spark debug info: appName {}, masterHost {}, namespace {}; host {}; mode {}; driver SA {}; " +
                        "executorsNum {}; hostName {}, gpuPerTask {}",
                appName, masterHost, namespace, host, deployMode, driverSa, executorsNum, hostName, gpuPerTask);

        return sparkContext(sparkSettings);
    }

    public static JavaSparkContext sparkContext(SparkSettings sparkSettings) {
        String host = sparkSettings.getHost();
        SparkConf sparkConf = new SparkConf(true)
                .setAppName(sparkSettings.getAppName())
                .setMaster(sparkSettings.getMasterHost())
                .setJars(sparkSettings.getJars())
                // Spark settings
                .set("spark.worker.cleanup.enabled", "true")
                .set("spark.submit.deployMode", sparkSettings.getDeployMode())
                .set("spark.executor.extraJavaOptions", "-DallowConventionalDistJar=true")
                .set("spark.driver.extraJavaOptions", "-DallowConventionalDistJar=true")
                // executors
                .set("spark.executor.instances", sparkSettings.getExecutorsNum())
                .set("spark.executor.cores", sparkSettings.getCores())
                .set("spark.executor.memory", sparkSettings.getMemory())
                .set("spark.executorEnv.NCCL_DEBUG", "INFO")
                .set("spark.task.resource.gpu.amount", sparkSettings.getGpuPerTask())
                .set("spark.executor.resource.gpu.discoveryScript", "/opt/sparkRapidsPlugin/getGpusResources.sh")
                .set("spark.executor.resource.gpu.amount", "1")
                .set("spark.executor.resource.gpu.vendor", "nvidia.com")
                // driver
                .set("spark.ui.enabled", "true")
                .set("spark.ui.port", "4040")
                .set("spark.driver.host", host)
                .set("spark.driver.bindAddress", host)
                .set("spark.driver.blockManager.port", "45029")
                .set("spark.driver.port", "33139")
                .set("spark.driver.resource.gpu.amount", "0")
                .set("spark.port.maxRetries", "16")
                .set("spark.driver.maxResultSize", "2g")
                .set("spark.executor.heartbeatInterval", "200000")
                .set("spark.network.timeout", "300000")
                // rapids
                .set("spark.rapids.force.caller.classloader", "false")
                .set("spark.rapids.memory.gpu.pooling.enabled", "false")
                .set("spark.rapids.memory.gpu.minAllocFraction", "0.0001")
                .set("spark.rapids.memory.gpu.reserve", "2")
                .set("spark.rapids.sql.enabled", "true")
                .set("spark.rapids.sql.explain", "NONE") //ALL for debug
                .set("spark.rapids.sql.hasNans", "false")
                .set("spark.rapids.sql.csv.read.float.enabled", "true")
                .set("spark.rapids.sql.castFloatToString.enabled", "true")
                .set("spark.rapids.sql.csv.read.double.enabled", "true")
                .set("spark.rapids.sql.castDoubleToString.enabled", "true")
                .set("spark.rapids.sql.exec.CollectLimitExec", "true")
                .set("spark.locality.wait", "0s")
                .set("spark.sql.files.maxPartitionBytes", "512m")
                .set("spark.sql.adaptive.enabled", "false")
                .set("spark.plugins", "com.nvidia.spark.SQLPlugin")
                // Cassandra
                .set("spark.jars.packages", "datastax:spark-cassandra-connector:3.3.0-s_2.12")
                .set("spark.sql.extensions", "com.datastax.spark.connector.CassandraSparkExtensions")
                .set("spark.cassandra.auth.username", sparkSettings.getCassandraUser())
                .set("spark.cassandra.auth.password", sparkSettings.getCassandraPass())
                .set("spark.cassandra.connection.host", sparkSettings.getCassandraHosts());

        if (sparkSettings.isKubernetesEnabled()) {
            sparkConf.set("spark.kubernetes.container.image", sparkSettings.getExecutorImage());

            if (sparkSettings.getDeployMode().equals("client")) {
                sparkConf
                    .set("spark.kubernetes.driver.pod.name", sparkSettings.getHostName())
                    .set("spark.kubernetes.namespace", sparkSettings.getNamespace())
                    .set("spark.kubernetes.authenticate.driver.serviceAccountName", sparkSettings.getDriverSa());
            }
        }
        return new JavaSparkContext(sparkConf);
    }
}
