package com.mlwebservice.config;

import com.mlwebservice.utils.SparkUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Slf4j
@Profile("cluster")
@Configuration
@RequiredArgsConstructor
public class SparkConfiguration {

    private final SparkSettings sparkSettings;

    @Bean
    public JavaSparkContext javaSparkContext() {
        return SparkUtils.javaSparkContext(sparkSettings);
    }

    @Bean
    public SparkSession sparkSession(JavaSparkContext context) {
        return SparkUtils.sparkSession(sparkSettings, context);
    }
}
