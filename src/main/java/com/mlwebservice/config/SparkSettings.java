package com.mlwebservice.config;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@Getter(AccessLevel.PUBLIC)
@Configuration
public class SparkSettings {

    public final String executorsNum;
    private final String appName;
    private final String masterHost;
    private final String namespace;
    private final String deployMode;
    private final String driverSa;
    private final String cores;
    private final String memory;
    private final String gpuPerTask;
    private final String hostName;
    private final String host;
    private final String cassandraUser;
    private final String cassandraPass;
    private final String cassandraHosts;
    private final String executorImage;
    private final String rapidsLibVersion;
    private final String[] jars;

    private final boolean kubernetesEnabled;

    public SparkSettings(@Value("${spring.application.name}") String appName,
                         @Value("${spark.masterHost}") String masterHost,
                         @Value("${spark.deployMode}") String deployMode,
                         @Value("${spark.executors.num}") String executorsNum,
                         @Value("${spark.executors.cores}") String cores,
                         @Value("${spark.executors.memory}") String memory,
                         @Value("${spark.executors.gpuPerTask}") String gpuPerTask,
                         @Value("${spark.kubernetes.namespace}") String namespace,
                         @Value("${spark.kubernetes.driverServiceAccount}") String driverSa,
                         @Value("${spark.jars}") String[] jars,
                         @Value("${spark.cassandra.auth.username}") String cassandraUser,
                         @Value("${spark.cassandra.auth.password}") String cassandraPass,
                         @Value("${spark.cassandra.connection.hosts}") String cassandraHosts,
                         @Value("${spark.kubernetes.image}") String executorImage,
                         @Value("${spark.rapids.version}") String rapidsLibVersion) throws UnknownHostException {
        this.appName = appName;
        this.masterHost = masterHost;
        this.deployMode = deployMode;
        this.executorsNum = executorsNum;
        this.cores = cores;
        this.memory = memory;
        this.gpuPerTask = gpuPerTask;
        this.namespace = namespace;
        this.driverSa = driverSa;
        this.jars = jars;

        this.host = InetAddress.getLocalHost().getHostAddress();
        this.hostName = InetAddress.getLocalHost().getHostName();

        this.cassandraUser = cassandraUser;
        this.cassandraPass = cassandraPass;
        this.cassandraHosts = cassandraHosts;

        this.executorImage = executorImage;
        this.rapidsLibVersion = rapidsLibVersion;
        this.kubernetesEnabled = masterHost.contains("k8s");
    }
}
