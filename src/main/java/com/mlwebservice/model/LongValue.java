package com.mlwebservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class LongValue implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long value;
}
