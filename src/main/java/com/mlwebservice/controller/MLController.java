package com.mlwebservice.controller;

import com.mlwebservice.service.KotlinXGBoostService;
import com.mlwebservice.service.RapidsService;
import com.mlwebservice.service.SparkMLService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class MLController {

    private final SparkMLService sparkMLService;
    private final RapidsService rapidsService;
    private final KotlinXGBoostService kotlinXGBoostService;

    @GetMapping("/forest")
    public ResponseEntity<?> sparkML() {
        sparkMLService.randomForestTest();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/gpu_test")
    public ResponseEntity<?> rapidsGpuTest() {
        rapidsService.testRapids();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/xgboost")
    public ResponseEntity<?> rapidsXGBoost() {
        kotlinXGBoostService.xgBoost();
        return ResponseEntity.ok().build();
    }
}
