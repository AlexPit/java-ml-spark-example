package com.mlwebservice.service

import com.mlwebservice.config.SparkSettings
import ml.dmlc.xgboost4j.scala.spark.XGBoostRegressionModel
import ml.dmlc.xgboost4j.scala.spark.XGBoostRegressor
import org.apache.spark.ml.PredictionModel
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.springframework.stereotype.Service
import scala.collection.immutable.HashMap
import scala.collection.immutable.Map

@Service
class KotlinXGBoostService(
    private val sparkSettings: SparkSettings,
    private val session: SparkSession
) {

    fun xgBoost() {
        val trainPath = "/opt/spark/train/train.parquet"
        val evalPath = "/opt/spark/eval/eval.parquet"

        val tdf = session.read().parquet(trainPath)
        val edf = session.read().parquet(evalPath)

        val labelName = "fare_amount"
        val featureColumns = arrayOf(
            "passenger_count", "trip_distance", "pickup_longitude", "pickup_latitude",
            "rate_code", "dropoff_longitude", "dropoff_latitude", "hour", "day_of_week", "is_weekend", "h_distance"
        )

        val map: Map<String, Any> = HashMap<String, Any>()
            .updated("learning_rate", 0.05)
            .updated("max_depth", 8)
            .updated("subsample", 0.8)
            .updated("gamma", 1)
            .updated("num_round", 500)
            .updated("tree_method", "gpu_hist")
            .updated("num_workers", sparkSettings.executorsNum)

        val regressor = XGBoostRegressor(map)
        regressor.setLabelCol(labelName)
        regressor.setFeaturesCol(featureColumns)

        val model: PredictionModel<Vector, XGBoostRegressionModel> = regressor.fit(tdf)
        val predictions = model.transform(edf)

        val result = predictions.withColumn("error", col("prediction").minus(col(labelName)))
        result.select(labelName, "prediction", "error").show()
        result.describe(labelName, "prediction", "error").show()
    }
}
