package com.mlwebservice.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.mlwebservice.domain.request.AnalyticsRequest
import com.mlwebservice.exception.ServiceException
import com.mlwebservice.persist.entity.ModelEntity
import com.mlwebservice.persist.repository.postgres.ModelRepository
import ml.dmlc.xgboost4j.scala.spark.XGBoostRegressionModel
import org.apache.logging.log4j.LogManager
import org.apache.spark.ml.PredictionModel
import org.apache.spark.ml.linalg.Vector
import org.springframework.stereotype.Service
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.*

@Service
class ModelService(
    private val modelRepository: ModelRepository,
    private val objectMapper : ObjectMapper
) {

    companion object {
        private val log = LogManager.getLogger(this::class.java.name)
    }

    fun saveModel(
        model : PredictionModel<Vector, XGBoostRegressionModel>,
        taskId : UUID,
        modelParameters : AnalyticsRequest.ModelParameters
    ) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        ObjectOutputStream(byteArrayOutputStream).use { it.writeObject(model) }
        val modelByteArray: ByteArray = byteArrayOutputStream.toByteArray()
        val jsonParams : JsonNode = objectMapper.convertValue(modelParameters, JsonNode::class.java)

        val entity = ModelEntity(modelByteArray, taskId, jsonParams)
        modelRepository.save(entity)
        log.info("Model for task id {} saved. Parameters map: {}, jsonNode: {}",
            taskId, modelParameters, jsonParams)
    }

    internal inline fun <reified T> loadModel(modelId: Long): T {
        val optional = modelRepository.findById(modelId)

        val entity = optional.get()
        val modelByteArray = entity.model

        val byteArrayInputStream = ByteArrayInputStream(modelByteArray)
        val modelObject = ObjectInputStream(byteArrayInputStream).use { it.readObject() }

        if (modelObject is T) {
            return modelObject
        } else {
            throw ServiceException.withMessage("Model id $modelId has incorrect format")
        }
    }
}
