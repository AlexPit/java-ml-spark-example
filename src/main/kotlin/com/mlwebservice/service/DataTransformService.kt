package com.mlwebservice.service

import com.mlwebservice.persist.repository.cassandra.impl.CombinedDataRepository
import org.apache.logging.log4j.LogManager
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

@Service
class DataTransformService(
    private val dataReaderService: DataReaderService,
    private val combinedDataRepository : CombinedDataRepository
) {

    companion object {
        private val log = LogManager.getLogger(this::class.java.name)
    }

    fun transformData(
        ticker : String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        offset : Long,
        batchSize : Int
    ) {
        var currentBatchOffset = 0
        var i = 0

        var tdf: Dataset<Row>?
        do {
            log.info("Data transformation: task {}, iteration {},: currentOffset {}",
                taskNumber,i + 1, currentBatchOffset)

            tdf = dataReaderService.getDatasetWithLabel(
                ticker, taskNumber, dateStart, dateEnd, offset, currentBatchOffset, batchSize
            )
            if (tdf.isEmpty) break

            combinedDataRepository.saveData(tdf.selectExpr(*DataReaderService.allColumns), ticker, taskNumber)
            currentBatchOffset += batchSize
            i++
        } while (tdf?.isEmpty == false)

        log.info("Data transformation of task {} completed", taskNumber)
    }
}
