package com.mlwebservice.service

import com.mlwebservice.config.SparkSettings
import com.mlwebservice.domain.request.AnalyticsRequest
import com.mlwebservice.domain.response.ModelTrainResult
import com.mlwebservice.domain.response.ModelTrainResultResponse
import com.mlwebservice.domain.response.StockPredictDto
import com.mlwebservice.persist.repository.cassandra.impl.CombinedDataRepository
import com.mlwebservice.service.DataReaderService.Companion.allColumns
import com.mlwebservice.service.DataReaderService.Companion.featureColumns
import com.mlwebservice.service.DataReaderService.Companion.labelName
import ml.dmlc.xgboost4j.scala.spark.XGBoostRegressionModel
import ml.dmlc.xgboost4j.scala.spark.XGBoostRegressor
import org.apache.logging.log4j.LogManager
import org.apache.spark.ml.PredictionModel
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col
import org.springframework.stereotype.Service
import scala.collection.immutable.HashMap
import scala.collection.immutable.Map
import java.time.LocalDate
import java.util.*

@Service
open class StockAnalyticsService(
    private val dataReaderService: DataReaderService,
    private val sparkSettings : SparkSettings,
    private val modelService : ModelService,
    private val combinedDataRepository : CombinedDataRepository
) {

    companion object {
        private val log = LogManager.getLogger(this::class.java.name)
        private val resultExp = arrayOf(
            "from_unixtime(dateTimeUnix) as dateTime",
            "close",
            "from_unixtime(labelDateTimeUnix) as labelDateTime",
            "label as realResult",
            "prediction",
            "error"
        )
    }

    fun incrementTrainModel(
        ticker : String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        evalPivotPoint : Long,
        offset : Long,
        batchSize : Int,
        modelParameters : AnalyticsRequest.ModelParameters
    ) : ModelTrainResultResponse {
        val pivot = dateEnd.minusDays(evalPivotPoint)
        var currentBatchOffset = 0
        var i = 0

        val modelParams = createModelParams(modelParameters)
        val regressor = xgBoostRegressor(modelParams)

        var model: PredictionModel<Vector, XGBoostRegressionModel>? = null
        var predictions: Dataset<Row>? = null

        var tdf: Dataset<Row>?
        do {
            log.info("Iteration {}: currentOffset {}", i, currentBatchOffset)
            tdf = dataReaderService.getDatasetWithLabel(
                ticker, taskNumber, dateStart, pivot, offset, currentBatchOffset, batchSize
            )
            if (tdf.isEmpty) break

            // todo change XGBoost for model which supports an iteration learning
            model = regressor.fit(tdf)
            combinedDataRepository.saveData(tdf.selectExpr(*allColumns), ticker, taskNumber)

            currentBatchOffset += batchSize
            i++
        } while (tdf?.isEmpty == false)

        val edf = dataReaderService.getDatasetWithLabel(
            ticker, taskNumber, pivot, dateEnd, offset, 0, 100).selectExpr(*allColumns)
        if (model != null) {
            predictions = model.transform(edf)
        }
        combinedDataRepository.saveData(edf.selectExpr(*allColumns), ticker, taskNumber)
        modelService.saveModel(model!!, taskNumber, modelParameters)

        val result = predictions!!.withColumn("error", col("prediction").minus(col(labelName)))
        return ModelTrainResultResponse(ModelTrainResult.listFromDataset(result.selectExpr(*resultExp)))
    }

    fun trainModel(
        ticker : String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        evalPivotPoint : Long,
        offset : Long,
        modelParameters : AnalyticsRequest.ModelParameters
    ) : ModelTrainResultResponse {
        val pivot = dateEnd.minusDays(evalPivotPoint)

        val tdf = dataReaderService.getDatasetWithLabel(ticker, taskNumber, dateStart, pivot, offset)
        val edf = dataReaderService.getDatasetWithLabel(ticker, taskNumber, pivot, dateEnd, offset)
            .selectExpr(*allColumns)

        val modelParams = createModelParams(modelParameters)
        val regressor = xgBoostRegressor(modelParams)

        val model: PredictionModel<Vector, XGBoostRegressionModel> = regressor.fit(tdf)
        val predictions = model.transform(edf)

        combinedDataRepository.saveData(tdf.selectExpr(*allColumns).unionAll(edf), ticker, taskNumber)
        modelService.saveModel(model, taskNumber, modelParameters)

        val result = predictions.withColumn("error", col("prediction").minus(col(labelName)))
        return ModelTrainResultResponse(ModelTrainResult.listFromDataset(result.selectExpr(*resultExp)))
    }

    fun predictWithExistingModel(
        ticker : String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        modelId : Long
    ): StockPredictDto {
        val model: PredictionModel<Vector, XGBoostRegressionModel> = modelService.loadModel(modelId)
        val data = dataReaderService.getMainDataset(ticker, taskNumber, dateStart, dateEnd)

        var predictions = model.transform(data)
        predictions = predictions.select("dateTime", "prediction")
        return StockPredictDto.fromDataset(predictions)
    }

    private fun xgBoostRegressor(modelParams: Map<String, Any>): XGBoostRegressor {
        val regressor = XGBoostRegressor(modelParams)
        regressor.setLabelCol(labelName)
        regressor.setFeaturesCol(featureColumns)
        return regressor
    }

    private fun createModelParams(modelParameters: AnalyticsRequest.ModelParameters): Map<String, Any> {
        val paramsMap: Map<String, Any> = HashMap<String, Any>()
            .updated("learning_rate", modelParameters.learningRate)
            .updated("max_depth", modelParameters.maxDepth)
            .updated("subsample", modelParameters.subSample)
            .updated("gamma", modelParameters.gamma)
            .updated("num_round", modelParameters.numRound)
            .updated("tree_method", modelParameters.treeMethod)
            .updated("num_workers", sparkSettings.executorsNum)
            .updated("refresh_leaf", modelParameters.refreshLeaf)
            .updated("process_type", modelParameters.processType)
            if (modelParameters.updater != null) {
                paramsMap.updated("updater", modelParameters.updater)
            }
        log.info("Model params: {}", paramsMap)
        return paramsMap
    }
}
