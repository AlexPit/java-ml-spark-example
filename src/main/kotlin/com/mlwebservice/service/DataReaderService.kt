package com.mlwebservice.service

import com.mlwebservice.persist.repository.cassandra.impl.*
import org.apache.logging.log4j.LogManager
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

private const val DATE_TIME = "datetime"
private const val LEFT_OUTER_JOIN = "leftouter"

@Service
class DataReaderService(
    private val timeSeriesRepository : TimeSeriesRepository,
    private val emaRepository : EmaIndicatorRepository,
    private val stochasticRepository : StochasticIndicatorRepository,
    private val bBandIndicatorRepository : BBandIndicatorRepository,
    private val macdRepository : MacdRepository,
    private val rsiRepository : RsiRepository,
    private val smaRepository : SmaRepository,
    private val willrRepository : WillrRepository
) {

    companion object{
        private val log = LogManager.getLogger(this::class.java.name)
        const val labelName = "label"
        val featureColumns = arrayOf(
            "emaTimePeriod",
            "ema",
            "fastKPeriod",
            "slowKPeriod",
            "slowDPeriod",
            "slowD",
            "slowK",
            "bbTimePeriod",
            "bbSd",
            "lowerBand",
            "middleBand",
            "upperBand",
            "signalPeriod",
            "fastPeriod",
            "slowPeriod",
            "macd",
            "macdHist",
            "macdSignal",
            "rsiTimePeriod",
            "rsi",
            "smaTimePeriod",
            "sma",
            "willrTimePeriod",
            "willr",
            "close"
        )
        val featuresAndLabel = arrayOf(*featureColumns, labelName)
        val allColumns = arrayOf("id", "dateTimeUnix", "labelDateTimeUnix", *featuresAndLabel)
    }

    fun getDatasetWithLabel(
        ticker: String,
        taskNumber: UUID,
        dateStart: LocalDate,
        dateEnd: LocalDate,
        offset : Long,
        currentOffset : Int,
        batchSize : Int
    ): Dataset<Row> {
        val mainDataset = getMainDataset(ticker, taskNumber, dateStart, dateEnd, currentOffset, batchSize)
        return datasetWithLabel(mainDataset, offset)
    }

    fun getDatasetWithLabel(
        ticker: String,
        taskNumber: UUID,
        dateStart: LocalDate,
        dateEnd: LocalDate,
        offset: Long,
    ): Dataset<Row> {
        val mainDataset = getMainDataset(ticker, taskNumber, dateStart, dateEnd)
        return datasetWithLabel(mainDataset, offset)
    }

    fun getMainDataset(
        ticker : String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate
    ) : Dataset<Row> {
        val timeSeries = timeSeriesRepository.getDataset(ticker, taskNumber, dateStart, dateEnd).`as`("ts")
        val emaDataSet = emaRepository.getEmaDataSet(ticker, dateStart, dateEnd).`as`("ema")
        val stochasticDataset = stochasticRepository.getStochasticDataSet(ticker, dateStart, dateEnd).`as`("stoch")
        val bBandsDataset = bBandIndicatorRepository.getBBandsDataSet(ticker, dateStart, dateEnd).`as`("bb")
        val macdDataset = macdRepository.getMacdDataSet(ticker, dateStart, dateEnd).`as`("macd")
        val rsiDataset = rsiRepository.getRsiDataSet(ticker, dateStart, dateEnd).`as`("rsi")
        val smaDataset = smaRepository.getSmaDataSet(ticker, dateStart, dateEnd).`as`("sma")
        val willrDataset = willrRepository.getWillrDataSet(ticker, dateStart, dateEnd).`as`("willr")

        return combineDatasets(
            timeSeries, emaDataSet, stochasticDataset, bBandsDataset, macdDataset, rsiDataset, smaDataset, willrDataset
        )
    }

    private fun datasetWithLabel(
        mainDataset: Dataset<Row>,
        offset: Long
    ): Dataset<Row> {
        val labelDataset = getLabelDatasetFromMain(mainDataset, offset)

        val combinedDataset = labelDataset
            .join(
                mainDataset, labelDataset.col("id_eval")
                    .equalTo(mainDataset.col("id")), LEFT_OUTER_JOIN
            )
            .withColumn("dateTimeUnix", functions.unix_timestamp(functions.col("dateTime")))
            .withColumn("labelDateTimeUnix", functions.unix_timestamp(functions.col("labelDateTime")))

        return combinedDataset
            .withColumn(
                "id",
                functions.coalesce(combinedDataset.col("id_eval"), combinedDataset.col("id"))
            )
            .drop("id_eval")
            .selectExpr(*allColumns)
            .filter(functions.col("dateTime").isNotNull)
            .orderBy(functions.asc("dateTime"))
    }

    private fun getLabelDatasetFromMain(
        mainDataset: Dataset<Row>,
        offset: Long
    ): Dataset<Row> {
        val windowSpec = Window.orderBy(functions.asc("dateTime"))
        val labelDateTimeColumn = functions.lead(mainDataset.col("dateTime"), offset.toInt()).over(windowSpec)
        val labelColumn = functions.lead(mainDataset.col("close"), offset.toInt()).over(windowSpec)

        val labelDataset = mainDataset
            .withColumn("labelDateTime", labelDateTimeColumn)
            .withColumn("label", labelColumn)
            .filter(functions.col("labelDateTime").isNotNull)
            .select("labelDateTime", "label")
            .orderBy("labelDateTime")

        return labelDataset
            .withColumn("id_eval", functions.row_number()
                .over(Window.orderBy(functions.asc("labelDateTime"))))
            .select("id_eval", "labelDateTime", "label")
    }

    private fun getMainDataset(
        ticker : String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        currentOffset : Int,
        batchSize : Int
    ) : Dataset<Row> {
        val timeSeries = timeSeriesRepository
            .getDataset(ticker, taskNumber, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("ts")
        val emaDataSet = emaRepository
            .getEmaDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("ema")
        val stochasticDataset = stochasticRepository
            .getStochasticDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("stoch")
        val bBandsDataset = bBandIndicatorRepository
            .getBBandsDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("bb")
        val macdDataset = macdRepository
            .getMacdDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("macd")
        val rsiDataset = rsiRepository
            .getRsiDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("rsi")
        val smaDataset = smaRepository
            .getSmaDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("sma")
        val willrDataset = willrRepository
            .getWillrDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .`as`("willr")

        return combineDatasets(
            timeSeries, emaDataSet, stochasticDataset, bBandsDataset, macdDataset, rsiDataset, smaDataset, willrDataset
        )
    }

    private fun combineDatasets(
        timeSeries: Dataset<Row>,
        emaDataSet: Dataset<Row>,
        stochasticDataset: Dataset<Row>,
        bBandsDataset: Dataset<Row>,
        macdDataset: Dataset<Row>,
        rsiDataset: Dataset<Row>,
        smaDataset: Dataset<Row>,
        willrDataset: Dataset<Row>
    ): Dataset<Row> {

        val result = timeSeries
            .join(emaDataSet, timeSeries.col(DATE_TIME)
                    .equalTo(emaDataSet.col(DATE_TIME)), LEFT_OUTER_JOIN)
            .join(stochasticDataset, timeSeries.col(DATE_TIME)
                    .equalTo(stochasticDataset.col(DATE_TIME)), LEFT_OUTER_JOIN)
            .join(bBandsDataset, timeSeries.col(DATE_TIME)
                    .equalTo(bBandsDataset.col(DATE_TIME)), LEFT_OUTER_JOIN)
            .join(macdDataset, timeSeries.col(DATE_TIME)
                    .equalTo(macdDataset.col(DATE_TIME)), LEFT_OUTER_JOIN)
            .join(rsiDataset, timeSeries.col(DATE_TIME)
                    .equalTo(rsiDataset.col(DATE_TIME)), LEFT_OUTER_JOIN)
            .join(smaDataset, timeSeries.col(DATE_TIME)
                    .equalTo(smaDataset.col(DATE_TIME)), LEFT_OUTER_JOIN)
            .join(willrDataset, timeSeries.col(DATE_TIME)
                    .equalTo(willrDataset.col(DATE_TIME)), LEFT_OUTER_JOIN)
            .selectExpr(
                "ts.dateTime as dateTime",
                "ema.emaTimePeriod as emaTimePeriod",
                "ema.ema as ema",
                "stoch.fastKPeriod as fastKPeriod",
                "stoch.slowKPeriod as slowKPeriod",
                "stoch.slowDPeriod as slowDPeriod",
                "stoch.slowD as slowD",
                "stoch.slowK as slowK",
                "bb.timePeriod as bbTimePeriod",
                "bb.sd as bbSd",
                "bb.lowerBand as lowerBand",
                "bb.middleBand as middleBand",
                "bb.upperBand as upperBand",
                "macd.signalPeriod as signalPeriod",
                "macd.fastPeriod as fastPeriod",
                "macd.slowPeriod as slowPeriod",
                "macd.macd as macd",
                "macd.macdHist as macdHist",
                "macd.macdSignal as macdSignal",
                "rsi.timePeriod as rsiTimePeriod",
                "rsi.rsi as rsi",
                "sma.timePeriod as smaTimePeriod",
                "sma.sma as sma",
                "willr.timePeriod as willrTimePeriod",
                "willr.willr as willr",
                "ts.close as close"
            )

        val windowSpec = Window.orderBy(functions.asc("dateTime"))
        return result
            .withColumn("id", functions.row_number().over(windowSpec))
            .filter(functions.col("dateTime").isNotNull)
    }
}
