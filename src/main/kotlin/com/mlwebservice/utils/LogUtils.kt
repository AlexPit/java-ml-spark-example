package com.mlwebservice.utils

import org.apache.logging.log4j.Logger
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row

/**
 * Use it only for debug cause [Dataset.collectAsList] is very expensive operation
 */
fun logDataset(dataset: Dataset<Row>, log: Logger) {
    logDataset("", dataset, 20, log)
}

/**
 * Use it only for debug cause [Dataset.collectAsList] is very expensive operation
 */
fun logDataset(message: String, dataset: Dataset<Row>, log: Logger) {
    logDataset(message, dataset, 20, log)
}

/**
 * Use it only for debug cause [Dataset.collectAsList] is very expensive operation
 */
fun logDataset(message: String, dataset: Dataset<Row>, limit: Int, log: Logger) {
    val rows = dataset.limit(limit).collectAsList()
    val columnNames = dataset.columns()
    val columnWidths = Array(columnNames.size) { index ->
        val maxLength =
            maxOf(columnNames[index].length, rows.maxOfOrNull { getColumnValueAsString(it, index)?.length ?: 0 } ?: 0)
        maxLength + 1
    }

    val separator = buildSeparator(columnWidths)

    log.info("{} {}", message, separator)
    log.info("{} {}", message, buildRow(columnNames, columnWidths))
    log.info("{} {}", message, separator)

    rows.forEach { row ->
        val rowValues = Array(row.length()) { index ->
            getColumnValueAsString(row, index)?.padEnd(columnWidths[index]) ?: "".padEnd(columnWidths[index])
        }
        log.info("{} {}", message, buildRow(rowValues, columnWidths))
    }

    log.info("{} {}", message, separator)
}

/**
 * handle another data types here if needed
 */
private fun getColumnValueAsString(row: Row, index: Int): String? {
    return when (val value = row.get(index)) {
        is String -> value
        is java.sql.Date -> value.toString()
        else -> value?.toString()
    }
}

private fun buildSeparator(columnWidths: Array<Int>): String {
    val separatorBuilder = StringBuilder("+")
    columnWidths.forEach { width ->
        separatorBuilder.append("-".repeat(width))
        separatorBuilder.append("+")
    }
    return separatorBuilder.toString()
}

private fun buildRow(values: Array<String>, columnWidths: Array<Int>): String {
    val rowBuilder = StringBuilder("|")
    values.forEachIndexed { index, value ->
        rowBuilder.append(value.padEnd(columnWidths[index] + 0))
        rowBuilder.append("|")
    }
    return rowBuilder.toString()
}
