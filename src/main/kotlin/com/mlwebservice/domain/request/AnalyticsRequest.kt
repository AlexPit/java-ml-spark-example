package com.mlwebservice.domain.request

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate
import java.util.UUID

data class AnalyticsRequest @JsonCreator constructor(
    @JsonProperty("ticker") val ticker : String,
    @JsonProperty("task_number") val taskNumber : UUID,
    @JsonProperty("date_start") val dateStart : LocalDate,
    @JsonProperty("date_end") val dateEnd : LocalDate,
    @JsonProperty("eval_pivot_point") val evalPivotPoint : Long,
    @JsonProperty("model_parameters") val modelParameters : ModelParameters,
    @JsonProperty("date_offset") val dateOffset : Long,
    @JsonProperty("batch_size") val batchSize : Int
) {
    data class ModelParameters @JsonCreator constructor(
        @JsonProperty("learning_rate") val learningRate : Double,
        @JsonProperty("max_depth") val maxDepth : Int,
        @JsonProperty("sub_sample") val subSample : Double,
        @JsonProperty("gamma") val gamma : Int,
        @JsonProperty("num_round") val numRound : Int,
        @JsonProperty("tree_method") val treeMethod : String,
        @JsonProperty(value = "refresh_leaf", defaultValue = "1") val refreshLeaf : Int,
        @JsonProperty(value = "process_type", defaultValue = "default") val processType : String,
        @JsonProperty("updater") val updater : String?
    )
}
