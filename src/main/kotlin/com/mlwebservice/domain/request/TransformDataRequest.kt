package com.mlwebservice.domain.request

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate
import java.util.*

data class TransformDataRequest @JsonCreator constructor(
    @JsonProperty("ticker") val ticker : String,
    @JsonProperty("task_number") val taskNumber : UUID,
    @JsonProperty("date_start") val dateStart : LocalDate,
    @JsonProperty("date_end") val dateEnd : LocalDate,
    @JsonProperty("date_offset") val dateOffset : Long,
    @JsonProperty("batch_size") val batchSize : Int
)
