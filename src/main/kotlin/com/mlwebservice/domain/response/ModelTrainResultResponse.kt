package com.mlwebservice.domain.response

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class ModelTrainResultResponse @JsonCreator constructor(
    @get:JsonProperty("result_list") val resultList : List<ModelTrainResult>
)
