package com.mlwebservice.domain.response

import java.time.LocalDate
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row

data class StockPredictDto(
    var date: LocalDate,
    var predict: Double
) {
    companion object {

        fun fromDataset(dataset: Dataset<Row>): StockPredictDto {
            return fromRow(dataset.first())
        }

        fun listFromDataset(dataset: Dataset<Row>): List<StockPredictDto> {
            val list = mutableListOf<StockPredictDto>()
            val iterator = dataset.toLocalIterator()
            while (iterator.hasNext()) {
                val row = iterator.next()
                list.add(fromRow(row))
            }
            return list
        }

        private fun fromRow(row: Row): StockPredictDto {
            return StockPredictDto(
                row.getDate(0).toLocalDate(),
                row.getDouble(1)
            )
        }
    }
}
