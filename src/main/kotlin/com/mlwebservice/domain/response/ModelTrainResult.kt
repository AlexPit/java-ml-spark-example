package com.mlwebservice.domain.response

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class ModelTrainResult @JsonCreator constructor(
    @get:JsonProperty("date_time") val dateTime : LocalDateTime,
    @get:JsonProperty("close") val close : Double,
    @get:JsonProperty("label_datetime") val labelDateTime : LocalDateTime,
    @get:JsonProperty("real_result") val realResult : Double,
    @get:JsonProperty("prediction") val prediction : Double,
    @get:JsonProperty("error") val error : Double
) {
    companion object {
        private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

        fun fromDataset(dataset: Dataset<Row>): ModelTrainResult {
            return fromRow(dataset.first())
        }

        fun listFromDataset(dataset: Dataset<Row>): List<ModelTrainResult> {
            val list = mutableListOf<ModelTrainResult>()
            val iterator = dataset.toLocalIterator()
            while (iterator.hasNext()) {
                val row = iterator.next()
                list.add(fromRow(row))
            }
            return list
        }

        private fun fromRow(row: Row): ModelTrainResult {
            return ModelTrainResult(
                LocalDateTime.parse(row.getString(0), formatter),
                row.getDouble(1),
                LocalDateTime.parse(row.getString(2), formatter),
                row.getDouble(3),
                row.getDouble(4),
                row.getDouble(5)
            )
        }
    }
}