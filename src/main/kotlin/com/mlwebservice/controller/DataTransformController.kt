package com.mlwebservice.controller

import com.mlwebservice.domain.request.TransformDataRequest
import com.mlwebservice.service.DataTransformService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/transform")
class DataTransformController(
    private val dataTransformService: DataTransformService
) {

    @PostMapping(
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun transform(@RequestBody request : TransformDataRequest) : ResponseEntity<Void> {
        dataTransformService.transformData(
            request.ticker,
            request.taskNumber,
            request.dateStart,
            request.dateEnd,
            request.dateOffset,
            request.batchSize
        )
        return ResponseEntity.ok().build()
    }
}