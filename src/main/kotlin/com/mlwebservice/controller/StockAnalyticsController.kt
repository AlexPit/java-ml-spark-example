package com.mlwebservice.controller

import com.mlwebservice.domain.request.AnalyticsRequest
import com.mlwebservice.domain.response.ModelTrainResultResponse
import com.mlwebservice.domain.response.StockPredictDto
import com.mlwebservice.service.StockAnalyticsService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.util.*

@RestController
@RequestMapping("/analytics")
class StockAnalyticsController(
    private val stockAnalyticsService: StockAnalyticsService
) {

    @PostMapping(
        path = ["/increment"],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun incrementTrainModel(@RequestBody request: AnalyticsRequest) : ResponseEntity<ModelTrainResultResponse> {
        val result = stockAnalyticsService.incrementTrainModel(
            request.ticker,
            request.taskNumber,
            request.dateStart,
            request.dateEnd,
            request.evalPivotPoint,
            request.dateOffset,
            request.batchSize,
            request.modelParameters
        )
        return ResponseEntity.ok(result)
    }

    @PostMapping(
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun trainModel(@RequestBody request: AnalyticsRequest) : ResponseEntity<ModelTrainResultResponse> {
        val result = stockAnalyticsService.trainModel(
            request.ticker,
            request.taskNumber,
            request.dateStart,
            request.dateEnd,
            request.evalPivotPoint,
            request.dateOffset,
            request.modelParameters
        )
        return ResponseEntity.ok(result)
    }

    @GetMapping(
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun predict(
        @RequestParam ticker : String,
        @RequestParam taskNumber : UUID,
        @RequestParam dateStart : String,
        @RequestParam dateEnd : String,
        @RequestParam modelId : Long
    ) : ResponseEntity<StockPredictDto> {
        val startDate = LocalDate.parse(dateStart)
        val endDate = LocalDate.parse(dateEnd)
        val result = stockAnalyticsService.predictWithExistingModel(ticker, taskNumber, startDate, endDate, modelId)
        return ResponseEntity.ok(result)
    }
}
