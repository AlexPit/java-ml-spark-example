package com.mlwebservice.persist.entity

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType::class)
@Table(name = "models", schema = "instrument_data")
data class ModelEntity constructor(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Lob
    @Type(type="org.hibernate.type.BinaryType")
    @Column(name = "model", nullable = false, columnDefinition = "bytea")
    val model: ByteArray,

    @Column(name = "created_at", nullable = false)
    val createdAt: LocalDateTime,

    @Column(name = "last_trained_at", nullable = false)
    val lastTrainedAt: LocalDateTime,

    @Column(name = "task_id", nullable = false)
    val taskId: UUID,

    @Type(type = "jsonb-node")
    @Column(name = "parameters", nullable = false)
    val parameters: JsonNode
) {
    constructor(
        model: ByteArray,
        createdAt: LocalDateTime,
        lastTrainedAt: LocalDateTime,
        taskId: UUID,
        parameters: JsonNode
    ) : this(null, model, createdAt, lastTrainedAt, taskId, parameters)

    constructor(model: ByteArray, taskId: UUID, params : JsonNode) : this(
        null, model, LocalDateTime.now(), LocalDateTime.now(), taskId,  params
    )

    constructor() : this(
        null, byteArrayOf(), LocalDateTime.now(), LocalDateTime.now(), UUID.randomUUID(),  emptyObjectNode()
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ModelEntity

        if (!model.contentEquals(other.model)) return false
        if (parameters != other.parameters) return false
        if (createdAt != other.createdAt) return false
        if (lastTrainedAt != other.lastTrainedAt) return false
        if (taskId != other.taskId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = model.contentHashCode()
        result = 31 * result + parameters.hashCode()
        result = 31 * result + createdAt.hashCode()
        result = 31 * result + lastTrainedAt.hashCode()
        result = 31 * result + taskId.hashCode()
        return result
    }
}

private fun emptyObjectNode(): ObjectNode {
    return ObjectMapper().createObjectNode()
}
