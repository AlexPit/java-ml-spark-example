package com.mlwebservice.persist.repository.postgres

import com.mlwebservice.persist.entity.ModelEntity
import org.springframework.data.repository.CrudRepository

interface ModelRepository : CrudRepository<ModelEntity, Long>