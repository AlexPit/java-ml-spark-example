package com.mlwebservice.persist.repository.cassandra

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession

abstract class AbstractCassandraRepository constructor(
    private val sparkSession: SparkSession
) {

    companion object {
        internal const val keyspace: String = "instrument_data"
    }

    fun cassandraDataset(keyspace: String, table: String): Dataset<Row> {
        val cassandraDataset: Dataset<Row> = sparkSession.read()
            .format("org.apache.spark.sql.cassandra")
            .option("keyspace", keyspace)
            .option("table", table)
            .load()

        cassandraDataset.createOrReplaceTempView(table)
        return cassandraDataset
    }

    fun cassandraDataset(table: String): Dataset<Row> {
        val cassandraDataset: Dataset<Row> = sparkSession.read()
            .format("org.apache.spark.sql.cassandra")
            .option("keyspace", keyspace)
            .option("table", table)
            .load()

        cassandraDataset.createOrReplaceTempView(table)
        return cassandraDataset
    }
}
