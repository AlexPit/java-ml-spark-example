package com.mlwebservice.persist.repository.cassandra.impl

import com.mlwebservice.persist.repository.cassandra.AbstractIndicatorRepository
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class MacdRepository(sparkSession: SparkSession)
    : AbstractIndicatorRepository(sparkSession, "macd_history") {

    fun getMacdDataSet(ticker: String,
                       dateStart : LocalDate,
                       dateEnd : LocalDate
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "fast_period as fastPeriod",
                "slow_period as slowPeriod",
                "signal_period as signalPeriod",
                "CAST(macd AS Double) as macd",
                "CAST(macd_hist AS Double) as macdHist",
                "CAST(macd_signal AS Double) as macdSignal"
            )

        dataset.createOrReplaceTempView("macd")
        return dataset
    }

    fun getMacdDataSet(ticker: String,
                       dateStart : LocalDate,
                       dateEnd : LocalDate,
                       currentOffset : Int,
                       batchSize : Int
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "fast_period as fastPeriod",
                "slow_period as slowPeriod",
                "signal_period as signalPeriod",
                "CAST(macd AS Double) as macd",
                "CAST(macd_hist AS Double) as macdHist",
                "CAST(macd_signal AS Double) as macdSignal"
            )

        dataset.createOrReplaceTempView("macd")
        return dataset
    }
}
