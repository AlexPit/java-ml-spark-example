package com.mlwebservice.persist.repository.cassandra.impl

import com.mlwebservice.persist.repository.cassandra.AbstractIndicatorRepository
import org.apache.spark.sql.SparkSession
import org.springframework.stereotype.Component
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.from_unixtime
import org.jetbrains.kotlinx.spark.api.lit
import java.util.*

@Component
class CombinedDataRepository(sparkSession: SparkSession)
    : AbstractIndicatorRepository(sparkSession, "combined_data") {

    fun saveData(
        dataset : Dataset<Row>,
        ticker : String,
        taskNumber : UUID
    ) {
        val modifiedDataset = dataset
            .withColumn("dateTime",
                from_unixtime(col("dateTimeUnix")))
            .selectExpr(
            "dateTime as datetime",
            "bbTimePeriod as bbands_time_period",
            "bbSd as sd",

            "emaTimePeriod as ema_time_period",
            "ema",

            "fastKPeriod as fast_k_period",
            "slowKPeriod as slow_k_period",
            "slowDPeriod as slow_d_period",
            "slowD as slow_d",
            "slowK as slow_k",

            "lowerBand as lower_band",
            "middleBand as middle_band",
            "upperBand as upper_band",

            "signalPeriod as macd_signal_period",
            "fastPeriod as macd_fast_period",
            "slowPeriod as macd_slow_period",
            "macd",
            "macdHist as macd_hist",
            "macdSignal as macd_signal",

            "rsiTimePeriod as rsi_time_period",
            "rsi",

            "smaTimePeriod as sma_time_period",
            "sma",

            "willrTimePeriod as willr_time_period",
            "willr",

            "close")
            .withColumn("ticker", lit(ticker))
            .withColumn("task_number", lit(taskNumber.toString()))
            .withColumn("timeframe", lit("1day"))
            .na().drop()

        saveDataSet(modifiedDataset)
    }
}