package com.mlwebservice.persist.repository.cassandra.impl

import com.mlwebservice.persist.repository.cassandra.AbstractIndicatorRepository
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class BBandIndicatorRepository(sparkSession: SparkSession)
    : AbstractIndicatorRepository(sparkSession, "bbands_history") {

    fun getBBandsDataSet(ticker: String,
                         dateStart : LocalDate,
                         dateEnd : LocalDate
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "time_period as timePeriod",
                "sd",
                "CAST(lower_band AS Double) as lowerBand",
                "CAST(middle_band AS Double) as middleBand",
                "CAST(upper_band AS Double) as upperBand"
            )

        dataset.createOrReplaceTempView("eval")
        return dataset
    }

    fun getBBandsDataSet(ticker: String,
                         dateStart : LocalDate,
                         dateEnd : LocalDate,
                         currentOffset : Int,
                         batchSize : Int
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "time_period as timePeriod",
                "sd",
                "CAST(lower_band AS Double) as lowerBand",
                "CAST(middle_band AS Double) as middleBand",
                "CAST(upper_band AS Double) as upperBand"
            )

        dataset.createOrReplaceTempView("eval")
        return dataset
    }
}