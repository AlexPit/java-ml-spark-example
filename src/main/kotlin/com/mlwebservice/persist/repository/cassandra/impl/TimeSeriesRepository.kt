package com.mlwebservice.persist.repository.cassandra.impl

import com.mlwebservice.persist.repository.cassandra.AbstractIndicatorRepository
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.util.*

@Component
class TimeSeriesRepository(
    sparkSession: SparkSession
) : AbstractIndicatorRepository(sparkSession, "time_series_history") {

    fun getDataset(
        ticker: String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, taskNumber, dateStart, dateEnd)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "CAST(close AS Double) as close"
            )

        dataset.createOrReplaceTempView("ts")
        return dataset
    }

    fun getDataset(
        ticker: String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        currentOffset : Int,
        batchSize : Int
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, taskNumber, dateStart, dateEnd, currentOffset, batchSize)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "CAST(close AS Double) as close"
            )

        dataset.createOrReplaceTempView("ts")
        return dataset
    }
}
