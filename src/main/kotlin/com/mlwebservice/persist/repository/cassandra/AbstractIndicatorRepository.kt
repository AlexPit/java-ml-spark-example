package com.mlwebservice.persist.repository.cassandra

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window.orderBy
import org.apache.spark.sql.functions
import java.time.LocalDate
import java.util.*

abstract class AbstractIndicatorRepository constructor(
    sparkSession: SparkSession,
    private val table : String
) : AbstractCassandraRepository(sparkSession) {

    fun getBaseDataSet(
        ticker: String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        currentOffset : Int,
        batchSize : Int
    ): Dataset<Row> {
        val filteredDataset = cassandraDataset(table)
            .filter(
                functions.col("ticker").equalTo(ticker)
                    .and(functions.col("task_number").equalTo(taskNumber.toString()))
                    .and(functions.col("datetime").between(dateStart, dateEnd))
            )

        val offsetDataset = filteredDataset.withColumn(
            "row_number",
            functions.row_number().over(orderBy("datetime"))
        )

        return offsetDataset
            .filter(functions.col("row_number")
                .between(currentOffset + 1, currentOffset + batchSize))
            .drop("row_number")
    }

    fun getBaseDataSet(
        ticker: String,
        dateStart : LocalDate,
        dateEnd : LocalDate,
        currentOffset : Int,
        batchSize : Int
    ): Dataset<Row> {
        val filteredDataset = cassandraDataset(table)
            .filter(
                functions.col("ticker").equalTo(ticker)
                    .and(functions.col("datetime").between(dateStart, dateEnd))
            )

        val offsetDataset = filteredDataset.withColumn(
            "row_number",
            functions.row_number().over(orderBy("datetime"))
        )

        return offsetDataset
            .filter(functions.col("row_number")
                .between(currentOffset + 1, currentOffset + batchSize))
            .drop("row_number")
    }

    fun getBaseDataSet(
        ticker: String,
        taskNumber : UUID,
        dateStart : LocalDate,
        dateEnd : LocalDate
    ): Dataset<Row> {
        return cassandraDataset(table)
            .filter(
                functions.col("ticker").equalTo(ticker)
                    .and(functions.col("task_number").equalTo(taskNumber.toString()))
                    .and(functions.col("datetime").between(dateStart, dateEnd)))
    }

    fun getBaseDataSet(
        ticker: String,
        dateStart : LocalDate,
        dateEnd : LocalDate
    ): Dataset<Row> {
        return cassandraDataset(table)
            .filter(
                functions.col("ticker").equalTo(ticker)
                    .and(functions.col("datetime").between(dateStart, dateEnd)))
    }

    open fun saveDataSet(dataset: Dataset<Row>) {
        dataset.write()
            .format("org.apache.spark.sql.cassandra")
            .mode("append")
            .option("confirm.truncate", "false")
            .option("keyspace", keyspace)
            .option("table", table)
            .save();
    }
}
