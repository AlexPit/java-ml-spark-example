package com.mlwebservice.persist.repository.cassandra.impl

import com.mlwebservice.persist.repository.cassandra.AbstractIndicatorRepository
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class StochasticIndicatorRepository(sparkSession: SparkSession)
    : AbstractIndicatorRepository(sparkSession, "stoch_history") {

    fun getStochasticDataSet(ticker: String,
                             dateStart : LocalDate,
                             dateEnd : LocalDate
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "fast_k_period as fastKPeriod",
                "slow_k_period as slowKPeriod",
                "slow_d_period as slowDPeriod",
                "CAST(slow_d AS Double) as slowD",
                "CAST(slow_k AS Double) as slowK"
            )

        dataset.createOrReplaceTempView("stoch")
        return dataset
    }

    fun getStochasticDataSet(ticker: String,
                             dateStart : LocalDate,
                             dateEnd : LocalDate,
                             currentOffset : Int,
                             batchSize : Int
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "fast_k_period as fastKPeriod",
                "slow_k_period as slowKPeriod",
                "slow_d_period as slowDPeriod",
                "CAST(slow_d AS Double) as slowD",
                "CAST(slow_k AS Double) as slowK"
            )

        dataset.createOrReplaceTempView("stoch")
        return dataset
    }
}