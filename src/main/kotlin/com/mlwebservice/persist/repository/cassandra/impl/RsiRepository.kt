package com.mlwebservice.persist.repository.cassandra.impl

import com.mlwebservice.persist.repository.cassandra.AbstractIndicatorRepository
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class RsiRepository(sparkSession: SparkSession)
    : AbstractIndicatorRepository(sparkSession, "rsi_history") {

    fun getRsiDataSet(ticker: String,
                      dateStart : LocalDate,
                      dateEnd : LocalDate
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "time_period as timePeriod",
                "CAST(rsi AS Double) as rsi"
            )

        dataset.createOrReplaceTempView("rsi")
        return dataset
    }

    fun getRsiDataSet(ticker: String,
                      dateStart : LocalDate,
                      dateEnd : LocalDate,
                      currentOffset : Int,
                      batchSize : Int
    ) : Dataset<Row> {
        val dataset = getBaseDataSet(ticker, dateStart, dateEnd, currentOffset, batchSize)
            .selectExpr(
                "to_date(datetime) as dateTime",
                "time_period as timePeriod",
                "CAST(rsi AS Double) as rsi"
            )

        dataset.createOrReplaceTempView("rsi")
        return dataset
    }
}
