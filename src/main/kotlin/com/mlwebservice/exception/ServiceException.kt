package com.mlwebservice.exception

class ServiceException private constructor(message: String) : RuntimeException(message) {

    companion object {
        private const val SERVICE_ERROR_MESSAGE = "Unhandled error: %s"

        fun withMessage(message: String): ServiceException {
            return ServiceException(String.format(SERVICE_ERROR_MESSAGE, message))
        }
    }
}
