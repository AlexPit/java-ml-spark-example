# Java ML Spark examples with DJL, Spark ML, XGBoost

Experimental project - an implementation of distributed ML system based on Java/Kotlin, Spark in K8S with GPU resources,
Cassandra, Postgres.

## Spark

Execute for build and push Docker images:

    ./build-cuda11.8-jdk-8-images.sh YOUR_DOCKER_REPO

Local worker docker container for standalone cluster:

    ./build-java-8-spark-3.3.2-local-docker.sh

You need to install standalone Spark 3.3.2 locally and run start-master.sh.

## Cassandra

Install Cassandra 4.1.1. Run migrations from `migrations/cassandra/` and import datasets from `migrations/datasets/`.

## Postgres

Install Postgres 15.3-1.pgdg110+1. Run migrations from `'migrations/postgresql/'`.

## Kubernetes

    kubectl apply -f kubernetes/dashboard.yml
    kubectl apply -f kubernetes/sa.yml
    kubectl apply -f kubernetes/spark-driver-crb.yml
    kubectl apply -f kubernetes/ml-app.yml
    kubectl -n kubernetes-dashboard create token admin-user
