#!/bin/bash
echo "=========== 1st stage ==========="
#docker build -f Dockerfile-cuda-java17 -t localhost:5000/cuda-jdk17:v1 .

echo "=========== 2nd stage ==========="
echo "### build Spark and PySpark images ###"
cd spark-3.3.2
./bin/docker-image-tool.sh -r localhost:5000 -t java-17-v1-3.3.2 -b java_image_tag=localhost:5000/cuda-jdk17:v1 -p ./kubernetes/dockerfiles/spark/bindings/python/Dockerfile -n build
docker tag localhost:5000/spark-py:java-17-v1-3.3.2 abvgdeej/cuda-jdk17-spark-py-3.3.2:v1
docker tag localhost:5000/spark:java-17-v1-3.3.2 abvgdeej/cuda-jdk17-spark-3.3.2:v1

echo "=========== 3rt stage ==========="
echo "### push Spark and PySpark images ###"
docker push abvgdeej/cuda-jdk17-spark-py-3.3.2:v1
docker push abvgdeej/cuda-jdk17-spark-3.3.2:v1
