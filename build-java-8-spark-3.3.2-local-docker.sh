#!/bin/bash
echo "=========== 1st stage ==========="
echo "### Build local Spark image ###"
docker build -t localhost:5000/java-8-3.3.2-docker \
              --build-arg java_image_tag=localhost:5000/spark:jdk8-3.3.2 \
              --progress=plain \
              -f Dockerfile-java-8-spark-3.3.2-local .

docker run --memory="6g" --cpus="4" --gpus all \
              --name=spark-3.3.2-jdk8 \
              -p 8081:8081 \
              -p 4041:4041 \
              -it -d \
              localhost:5000/java-8-3.3.2-docker
