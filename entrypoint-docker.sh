#!/bin/bash

SPARK_DRIVER_BIND_ADDRESS=192.168.145.128:7077
NCCL_DEBUG=INFO

source ~/.bashrc

start-worker.sh spark://$SPARK_DRIVER_BIND_ADDRESS
tail -f /dev/null
