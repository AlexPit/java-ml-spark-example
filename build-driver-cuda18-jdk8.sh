#!/bin/bash
export DOCKER_REPO="$1"
echo "Docker repo: $DOCKER_REPO"

mvn clean install -DskipTests=true

docker stop ML
docker rm ML
docker build -f Dockerfile --build-arg JAR_FILE=target/service.jar -t localhost:5000/ml:1 .
docker tag localhost:5000/ml:1 $DOCKER_REPO/ml:cuda11.8-jdk8
docker push $DOCKER_REPO/ml:cuda11.8-jdk8

#docker run --gpus all \
#        -p 9090:9090 \
#        -p 5005:5005 \
#        -p 4040:4040 \
#        -p 33139-33155:33139-33155 \
#        -p 45029-45045:45029-45045 \
#        -e SPARK_EXECUTORS=1 -e SPARK_MASTER=spark://192.168.145.128:7077 \
#        -e CASSANDRA_HOST=192.168.0.105 \
#        -e POSTGRES_HOST=192.168.0.105 \
#        -e POSTGRES_USER=superadmin \
#        -e POSTGRES_PASS=123456 \
#        --name=ML \
#        -it -d localhost:5000/ml:1
